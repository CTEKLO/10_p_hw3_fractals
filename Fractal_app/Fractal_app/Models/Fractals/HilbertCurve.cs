﻿using Fractal_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;

namespace Fractal_app.Models.Fractals
{
    internal class HilbertCurve : Fractal
    {
        internal int Size { get; private set; }

        private int X { get; set; }
        private int Y { get; set; }

        internal override double Progress { get; set; }

        /// <summary>
        /// Main method for drawing.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="depth"> Recursion depth </param>
        /// <param name="startColor"> Start color </param>
        /// <param name="endColor"> End color </param>
        /// <param name="parameters"> Parameters </param>
        /// <param name="ct"> info about thread cancelling </param>
        internal override void Draw(CanvasViewModel canvas, int depth, Color startColor, Color endColor,
            Dictionary<string, object> parameters, CancellationToken ct)
        {
            Progress = 0;

            Depth = depth;
            X = Y = (int)parameters["size"];

            Size = (int)parameters["size"] / 50;
            StartColor = startColor;
            EndColor = endColor;

            var depthRate = 0;
            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depthRate);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depthRate);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depthRate);
            SolidColorBrush brush = null;

            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });

            Rotate0(canvas, Depth, ct);
        }

        private void AddLine(CanvasViewModel canvas, Brush brush, int lx, int ly, CancellationToken ct)
        {
            Line line = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                line = new Line
                {
                    Stroke = brush,
                    X1 = X,
                    Y1 = Y,
                    StrokeThickness = Size / 10,
                    X2 = X += lx,
                    Y2 = Y += ly
                };
            });

            // High performance.
            if (ct.IsCancellationRequested)
                ct.ThrowIfCancellationRequested();

            canvas.AddShape(line);
        }

        /// <summary>
        /// Draw figure
        /// 1------------|
        ///              |
        ///              |
        ///              |
        /// -------------|
        /// 
        /// 1 - start point.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="depth"> Depth </param>
        /// <param name="ct"></param>
        private void Rotate90(CanvasViewModel canvas, int depth, CancellationToken ct)
        {
            if (depth == 0)
                return;

            Progress += 3 / (Math.Pow(4, Depth) - 1);

            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depth);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depth);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depth);
            SolidColorBrush brush = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });

            Rotate180(canvas, depth - 1, ct);
            AddLine(canvas, brush, Size, 0, ct);

            Rotate90(canvas, depth - 1, ct);
            AddLine(canvas, brush, 0, Size, ct);

            Rotate90(canvas, depth - 1, ct);
            AddLine(canvas, brush, -Size, 0, ct);
            Rotate0(canvas, depth - 1, ct);
        }

        /// <summary>
        /// Draw figure
        /// |------------
        /// |
        /// |
        /// |
        /// |------------1
        /// 
        /// 1 - start point.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="depth"> Depth </param>
        /// <param name="ct"></param>
        private void Rotate270(CanvasViewModel canvas, int depth, CancellationToken ct)
        {
            if (depth == 0)
                return;

            Progress += 3 / (Math.Pow(4, Depth) - 1);

            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depth);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depth);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depth);
            SolidColorBrush brush = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });
            Rotate0(canvas, depth - 1, ct);
            AddLine(canvas, brush, -Size, 0, ct);

            Rotate270(canvas, depth - 1, ct);
            AddLine(canvas, brush, 0, -Size, ct);

            Rotate270(canvas, depth - 1, ct);
            AddLine(canvas, brush, Size, 0, ct);
            Rotate180(canvas, depth - 1, ct);
        }


        /// <summary>
        /// Draw figure
        /// |----------|
        /// |          |
        /// |          |
        /// |          |
        /// |          1
        /// 
        /// 1 - start point.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="depth"> Depth </param>
        /// <param name="ct"></param>
        private void Rotate0(CanvasViewModel canvas, int depth, CancellationToken ct)
        {
            if (depth == 0)
                return;

            Progress += 3 / (Math.Pow(4, Depth) - 1);

            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depth);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depth);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depth);
            SolidColorBrush brush = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });
            Rotate270(canvas, depth - 1, ct);
            AddLine(canvas, brush, 0, -Size, ct);

            Rotate0(canvas, depth - 1, ct);
            AddLine(canvas, brush, -Size, 0, ct);

            Rotate0(canvas, depth - 1, ct);
            AddLine(canvas, brush, 0, Size, ct);
            Rotate90(canvas, depth - 1, ct);
        }

        /// <summary>
        /// Draw figure
        /// 1         |
        /// |         |
        /// |         |
        /// |         |
        /// |---------|
        /// 
        /// 1 - start point.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="depth"> Depth </param>
        /// <param name="ct"></param>
        private void Rotate180(CanvasViewModel canvas, int depth, CancellationToken ct)
        {
            if (depth == 0)
                return;

            Progress += 3 / (Math.Pow(4, Depth) - 1);

            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depth);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depth);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depth);
            SolidColorBrush brush = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });

            Rotate90(canvas, depth - 1, ct);
            AddLine(canvas, brush, 0, Size, ct);
            Rotate180(canvas, depth - 1, ct);
            AddLine(canvas, brush, Size, 0, ct);
            Rotate180(canvas, depth - 1, ct);
            AddLine(canvas, brush, 0, -Size, ct);
            Rotate270(canvas, depth - 1, ct);
        }
    }
}
