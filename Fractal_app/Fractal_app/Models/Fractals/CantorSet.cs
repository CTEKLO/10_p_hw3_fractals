﻿using System;
using Fractal_app.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Fractal_app.Models
{
    internal class CantorSet : Fractal
    {
        internal int RectangleStartWidth { get; private set; }
        internal int RectangleStartHeight { get; private set; }
        internal int RectangleDistanceBetween { get; private set; }
        internal int Coefficient { get; private set; }

        internal override double Progress { get; set; }

        /// <summary>
        /// Draw fractal.
        /// </summary>
        /// <param name="canvas"> Field for drawing </param>
        /// <param name="depth"> Recursion depth </param>
        /// <param name="startColor"> Start color </param>
        /// <param name="endColor"> End color </param>
        /// <param name="parameters"> Parameters of fractal </param>
        /// <param name="ct"></param>
        internal override void Draw(CanvasViewModel canvas, int depth, Color startColor, Color endColor,
            Dictionary<string, object> parameters, CancellationToken ct)
        {

            Progress = 0;

            Depth = depth;
            RectangleStartWidth = (int)parameters["rectangleStartWidth"];
            RectangleStartHeight = (int)parameters["rectangleStartHeight"];
            RectangleDistanceBetween = RectangleStartHeight * 2;
            Coefficient = (int)parameters["coefficient"];

            StartColor = startColor;
            EndColor = endColor;

            DrawLayer(canvas, 0, 0, RectangleStartWidth, 0, ct);
        }

        /// <summary>
        /// Fill list of shapes.
        /// </summary>
        /// <param name="canvas"> Field for drawing </param>
        /// <param name="x"> X coordinate of rectangle (left upper corner) </param>
        /// <param name="y"> Y coordinate of rectangle (left upper corner) </param>
        /// <param name="rectangleWidth"> Width of rectangle </param>
        /// <param name="curDepth"> Current depth </param>
        /// <param name="ct"> Thread cancellation information </param>
        private void DrawLayer(CanvasViewModel canvas, int x, int y,
            int rectangleWidth, int curDepth, CancellationToken ct)
        {
            // High performance
            if (ct.IsCancellationRequested)
                ct.ThrowIfCancellationRequested();

            // Break condition.
            if (curDepth++ >= Depth)
                return;

            // Color gradient.
            var depthRate = (double)curDepth / Depth;
            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depthRate);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depthRate);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depthRate);
            Brush brush = null;

            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });

            // Create new rectangle.
            Rectangle rectangle = null;

            Progress += 1 / (Math.Pow(2, Depth) - 1);

            var y1 = y;
            Application.Current.Dispatcher.Invoke(() =>
            {
                rectangle = new Rectangle
                {
                    Stroke = brush,
                    Fill = brush,
                    Width = rectangleWidth,
                    Height = RectangleStartHeight
                };
                Canvas.SetLeft(rectangle, x);
                Canvas.SetTop(rectangle, y1);
            });

            // Add rectangle to list.
            canvas.AddShape(rectangle);

            // Distance between rectangle (y coordinate)
            y += RectangleDistanceBetween;

            // Recursive call.
            DrawLayer(canvas, x, y, rectangleWidth / Coefficient, curDepth, ct);
            DrawLayer(canvas,
                x + rectangleWidth * (int)(Coefficient * (1 - 1.0 / Coefficient)) / Coefficient,
                    y, rectangleWidth / Coefficient, curDepth, ct);
        }
    }
}
