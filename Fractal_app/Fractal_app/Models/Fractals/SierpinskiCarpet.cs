﻿using Fractal_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace Fractal_app.Models.Fractals
{
    internal class SierpinskiCarpet : Fractal
    {
        private int SideLength { get; set; }
        internal override double Progress { get; set; }

        /// <summary>
        /// Main method for drawing.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="depth"> Depth </param>
        /// <param name="startColor"> Start color </param>
        /// <param name="endColor"> End color </param>
        /// <param name="parameters"> Parameters </param>
        /// <param name="ct"></param>
        internal override void Draw(CanvasViewModel canvas, int depth, Color startColor,
            Color endColor, Dictionary<string, object> parameters, CancellationToken ct)
        {
            Progress = 0;

            Depth = depth + 1;
            StartPosition = (PointF)parameters["startPosition"];
            SideLength = (int)parameters["sideLength"];
            StartColor = startColor;
            EndColor = endColor;

            DrawLayer(canvas, StartPosition, SideLength, 0, ct);
        }

        /// <summary>
        /// Fill list of shapes.
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="leftUpCorner"> Left upper corner </param>
        /// <param name="sideLength"> Side length </param>
        /// <param name="curDepth"> Current depth </param>
        /// <param name="ct"></param>
        private void DrawLayer(CanvasViewModel canvas, PointF leftUpCorner,
            int sideLength, int curDepth, CancellationToken ct)
        {
            // High performance
            if (ct.IsCancellationRequested)
                ct.ThrowIfCancellationRequested();

            // Color gradient.
            var depthRate = (double)curDepth / Depth;
            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depthRate);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depthRate);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depthRate);
            Brush brush = null;

            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });

            if (curDepth == 0)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    AddSquare(canvas, leftUpCorner, sideLength, brush);
                });
            }

            // Break condition.
            if (curDepth >= Depth)
            {
                return;
            }

            // Divide the square into 9 parts.
            var newSideLength = sideLength / 3;
            IEnumerable<PointF> points = GetPoints(leftUpCorner, newSideLength);

            Progress += 7 / (Math.Pow(8, Depth) - 1);
            Application.Current.Dispatcher.Invoke(() =>
            {
                AddSquare(canvas, leftUpCorner, sideLength, brush);
            });

            foreach (var point in points)
            {
                DrawLayer(canvas, point, newSideLength, curDepth + 1, ct);
            }
        }

        /// <summary>
        /// Get array of points.
        /// </summary>
        /// <param name="leftUpCorner"> Left upper corner </param>
        /// <param name="sideLength"> Side length </param>
        /// <returns> Array of points </returns>
        private static IEnumerable<PointF> GetPoints(PointF leftUpCorner, int sideLength)
        {
            var leftUpCornerX = leftUpCorner.X;
            var x2 = leftUpCornerX + sideLength;
            var x3 = leftUpCornerX + 2 * sideLength;

            var leftUpCornerY = leftUpCorner.Y;
            var y2 = leftUpCornerY + sideLength;
            var y3 = leftUpCornerY + 2 * sideLength;

            var points = new PointF[8];

            points[0] = new PointF(leftUpCornerX, leftUpCornerY);
            points[1] = new PointF(x2, leftUpCornerY);
            points[2] = new PointF(x3, leftUpCornerY);

            points[3] = new PointF(leftUpCornerX, y2);
            points[4] = new PointF(x3, y2);
            points[5] = new PointF(leftUpCornerX, y3);

            points[6] = new PointF(x2, y3);
            points[7] = new PointF(x3, y3);

            return points;
        }

        /// <summary>
        /// Add square.
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="leftUpCorner"> Left upper corner </param>
        /// <param name="sideLength"> Side length </param>
        /// <param name="brush"> Brush </param>
        private static void AddSquare(CanvasViewModel canvas, PointF leftUpCorner, int sideLength,
            Brush brush)
        {
            var square = new Rectangle
            {
                Stroke = brush,
                Fill = brush,
                Width = sideLength,
                Height = sideLength
            };

            Canvas.SetTop(square, leftUpCorner.Y);
            Canvas.SetLeft(square, leftUpCorner.X);

            canvas.AddShape(square);
        }
    }
}
