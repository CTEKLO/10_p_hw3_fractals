﻿using Fractal_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace Fractal_app.Models
{
    internal class TSquare : Fractal
    {
        internal int SideLength { get; set; }
        internal override double Progress { get; set; }

        internal override void Draw(CanvasViewModel canvas, int depth, Color startColor, Color endColor,
            Dictionary<string, object> parameters, CancellationToken ct)
        {
            Progress = 0;
            Depth = depth;
            StartPosition = (PointF)parameters["startPosition"];

            StartColor = startColor;
            EndColor = endColor;
            SideLength = (int)parameters["sideLength"];

            DrawLayer(canvas, StartPosition, SideLength, 0, ct);
        }

        /// <summary>
        /// Add square.
        /// </summary>
        /// <param name="canvas"> Canvas </param>
        /// <param name="leftUpCorner"> left upper corner of square </param>
        /// <param name="sideLength"> Side length </param>
        /// <param name="brush"> Brush </param>
        private static void AddSquare(CanvasViewModel canvas, PointF leftUpCorner,
            int sideLength, Brush brush)
        {
            var square = new Rectangle
            {
                Stroke = brush,
                Fill = brush,
                Width = sideLength,
                Height = sideLength
            };

            Canvas.SetTop(square, leftUpCorner.Y);
            Canvas.SetLeft(square, leftUpCorner.X);

            canvas.AddShape(square);
        }

        private void DrawLayer(CanvasViewModel canvas, PointF leftUpCorner,
            int sideLength, int curDepth, CancellationToken ct)
        {
            // High performance
            if (ct.IsCancellationRequested)
                ct.ThrowIfCancellationRequested();

            // Color gradient.
            var depthRate = (double)curDepth / Depth;
            var colorR = (byte)(StartColor.R + (EndColor.R - StartColor.R) * depthRate);
            var colorG = (byte)(StartColor.G + (EndColor.G - StartColor.G) * depthRate);
            var colorB = (byte)(StartColor.B + (EndColor.B - StartColor.B) * depthRate);
            Brush brush = null;

            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = new SolidColorBrush(Color.FromRgb(colorR, colorG, colorB));
            });

            // Draw primary square.
            if (curDepth == 0)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    AddSquare(canvas, leftUpCorner, sideLength, brush);
                });
            }

            // Exit condition.
            if (curDepth++ >= Depth)
            {
                return;
            }

            // Array from left upper corners.
            PointF[] corners = GetCorners(leftUpCorner, sideLength);

            for (var i = 0; i < 4; i++)
            {
                DrawLayer(canvas, corners[i], sideLength / 2, curDepth, ct);
            }

            Progress += 3 / (Math.Pow(4, Depth) - 1);
            Application.Current.Dispatcher.Invoke(() =>
            {
                AddSquare(canvas, leftUpCorner, sideLength, brush);
            });
        }

        /// <summary>
        /// Get corners.
        /// </summary>
        /// <param name="leftUpCorner"> Left upper corner </param>
        /// <param name="sideLength"> Side length </param>
        /// <returns> Array of corners </returns>
        private static PointF[] GetCorners(PointF leftUpCorner, int sideLength)
        {
            var corners = new PointF[4];

            var quarter = (float)(sideLength / 4.0);

            for (var i = 0; i < 4; i++)
            {
                corners[i] = new PointF();
            }

            // Upper left square.
            corners[0].X = leftUpCorner.X - quarter;
            corners[0].Y = leftUpCorner.Y - quarter;

            // Bottom left square.
            corners[1].X = leftUpCorner.X - quarter;
            corners[1].Y = leftUpCorner.Y - quarter + sideLength;

            // Upper right square.
            corners[2].X = leftUpCorner.X - quarter + sideLength;
            corners[2].Y = leftUpCorner.Y - quarter;

            // Bottom right square.
            corners[3].X = leftUpCorner.X - quarter + sideLength;
            corners[3].Y = leftUpCorner.Y - quarter + sideLength;

            return corners;
        }
    }
}
