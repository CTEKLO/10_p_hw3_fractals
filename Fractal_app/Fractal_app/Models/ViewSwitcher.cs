﻿using Fractal_app.ViewModels;
using System;

namespace Fractal_app.Models
{
    internal static class ViewSwitcher
    {
        public static MainWindow ControlWindow { get; private set; }

        public static void Setup(MainWindow window) =>
            ControlWindow = window;

        public static void Switch(string view)
        {
            switch (view)
            {
                case "Start":
                    ControlWindow.DataContext = new StartViewModel();
                    break;
                case "Constructor":
                    ControlWindow.DataContext = new FractalCreatorViewModel();
                    break;
                default:
                    throw new ArgumentException("Bad type of chosen view");
            }
        }
    }
}
