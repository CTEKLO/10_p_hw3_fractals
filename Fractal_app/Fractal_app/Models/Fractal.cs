﻿using Fractal_app.ViewModels;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using Color = System.Windows.Media.Color;

namespace Fractal_app.Models
{
    internal abstract class Fractal
    {
        private protected Color StartColor { get; set; }
        private protected Color EndColor { get; set; }
        private protected int Depth { get; set; }
        private protected PointF StartPosition { get; set; }

        internal abstract double Progress { get; set; }
        internal abstract void Draw(CanvasViewModel canvas, int depth, Color startColor, Color endColor,
             Dictionary<string, object> parameters, CancellationToken ct);
    }
}
