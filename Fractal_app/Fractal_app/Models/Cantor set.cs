﻿using System;
using System.Windows.Shapes;
using System.Windows.Media;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fractal_app.Models
{
    class Cantor_set
    {
        
        //private void OnClick(object sender, RoutedEventArgs e)
        //{
            // ВВОДИТ ПОЛЬЗОВАТЕЛЬ (СДЕЛАЙ TEXTBOX)
            //int a = 400;

            // НАЧАЛЬНОЕ ПОЛОЖЕНИЕ НА КАНВАСЕ. 
            // МОЖНО БУДЕТ ПОДОГНАТЬ НУЖНОЕ + ПОДСТРОИТЬ, ЧТОБЫ
            // ОНО ЗАВИСЕЛО ОТ РАЗМЕРОВ ОКНА
            //int x = 610 / 2 - a / 2;

            // ЭТО КАКУЮ ЧАСТЬ ОТ ПРЯМОУГОЛЬНИКА МЫ БЕРЁМ 
            // В ДАННОМ СЛУЧАЕ 1/5 (НО ЭТО ТОЖЕ РЕШАЕТ ЮЗЕР)
            //int coef = 5;

            // Depth (ВЫБИРАЕТ ЮЗЕР)
            //int counter = 10;

            // Start drawing.
            //Draw(x, 100, a, counter, 0, coef);
        //}

        List<Shape> shapes = new List<Shape>();

        private List<Shape> Draw(int x, int y,
            int rectangleWidth, int reqDepth, int curDepth, int coef)
        {
            // Break condition
            if (curDepth >= reqDepth)
                return shapes;

            // Create new rectangle
            Rectangle rect = new Rectangle
            {
                // Set colors.
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(Colors.Black),
                Width = rectangleWidth,
                Height = 10
            };

            // Add rectangle to list.
            shapes.Add(rect);

            #region Drawing on canvas.

            // Set position on canvas.
            //Canvas.SetLeft(rect, x);
            //Canvas.SetTop(rect, y);

            // Add to canvas.
            //myCanvas.Children.Add(rect);

            #endregion

            curDepth++;

            // Distance between rectangle (y coord)
            y += 20;

            // Draw fractal.
            Draw(x, y, rectangleWidth / coef, reqDepth, curDepth, coef);
            Draw(x + (rectangleWidth * (int)(coef * (1 - 1.0 / coef)) / coef),
                y, rectangleWidth / coef, reqDepth, curDepth, coef);

            // Вот это вроде надо прописать, чтобы возвращало лист везде
            // (вижла ругается)
            // Надеюсь, оно не помешает работе.
            return shapes;
        }
    }
}
