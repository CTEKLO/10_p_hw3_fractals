﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace Fractal_app.ViewModels
{
    public class CanvasViewModel : INotifyPropertyChanged
    {
        public Canvas Area { get; set; }

        private int _size = 5000;
        public int Size
        {
            get => _size;
            set
            {
                if (_size == value)
                    return;
                _size = value;
                OnPropertyChanged();
            }
        }

        public CanvasViewModel()
        {
            Area = new Canvas();
        }

        public void AddShape(UIElement element)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Area.Children.Add(element);
            });
        }

        public void Clear()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Area.Children.Clear();
            });
        }

        public void Hide()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Area.Opacity = 0.1;
            });
        }

        public void Show()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Area.Opacity = 1;
            });
        }

        #region INotify.

        /// <summary>
        /// INotify implementation.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        internal void OnPropertyChanged([CallerMemberName]string prop = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        #endregion
    }
}
