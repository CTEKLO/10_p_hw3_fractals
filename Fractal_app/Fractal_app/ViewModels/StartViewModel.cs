﻿using Fractal_app.Models;
using Fractal_app.Views;
using MaterialDesignThemes.Wpf;
using System.Windows.Input;

namespace Fractal_app.ViewModels
{
    public class StartViewModel
    {
        public ICommand OpenAboutUsDialog => new CommandImplementation(AboutUsDialogOpen);
        public ICommand SwitchToConstructor => new CommandImplementation(SwitchToFractalCreator);
        private static async void AboutUsDialogOpen(object obj)
        {
            var aboutUsViewModel = new AboutUsViewModel();
            var aboutUsView = new AboutUsView
            {
                DataContext = aboutUsViewModel
            };

            await DialogHost.Show(aboutUsView);
        }

        private static void SwitchToFractalCreator(object obj) =>
            ViewSwitcher.Switch("Constructor");
    }
}
