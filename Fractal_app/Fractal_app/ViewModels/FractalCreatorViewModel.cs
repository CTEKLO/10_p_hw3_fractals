﻿using Fractal_app.Models;
using Fractal_app.Models.Fractals;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = System.Windows.Media.Color;
using ColorConverter = System.Windows.Media.ColorConverter;

namespace Fractal_app.ViewModels
{
    public class FractalCreatorViewModel : INotifyPropertyChanged
    {
        #region Fields.
        public ICommand Save => new CommandImplementation(SaveToFile);
        public ICommand Back => new CommandImplementation(SwitchToStart);
        public ObservableCollection<string> FractalTypes { get; }

        public CanvasViewModel Canvas { get; }
        public SnackbarMessageQueue MessageQueue { get; }

        public ObservableCollection<string> CustomBox { get; }

        private bool _customBoxEnabled;
        public bool CustomBoxEnabled
        {
            get => _customBoxEnabled;
            set
            {
                if (_customBoxEnabled == value)
                    return;
                _customBoxEnabled = value;

                OnPropertyChanged();
            }
        }

        private int _progressValue;
        public int ProgressValue
        {
            get => _progressValue;
            set
            {
                if (_progressValue == value)
                    return;
                _progressValue = value;

                OnPropertyChanged();
            }
        }

        private int _customBoxIndex;
        public int CustomBoxIndex
        {
            get => _customBoxIndex;
            set
            {
                if (_customBoxIndex == value)
                    return;
                _customBoxIndex = value;

                OnPropertyChanged();
                RedrawCanvas();
            }
        }

        private double _progressOpacity;
        public double ProgressOpacity
        {
            get => _progressOpacity;
            set
            {
                if (_progressOpacity == value)
                    return;
                _progressOpacity = value;

                OnPropertyChanged();
            }
        }

        private int _canvasSize = 600;
        public int CanvasSize
        {
            get => _canvasSize;
            set
            {
                if (_canvasSize == value)
                    return;
                _canvasSize = value;

                OnPropertyChanged();

                Canvas.Size = _canvasSize;
                RedrawCanvas();
            }
        }

        private int _fractalTypeIndex;
        public int FractalTypeIndex
        {
            get => _fractalTypeIndex;
            set
            {
                if (_fractalTypeIndex == value)
                    return;
                _fractalTypeIndex = value;

                OnPropertyChanged();
                RedrawCanvas();
            }
        }

        private int _fractalDepth = 1;
        public int FractalDepth
        {
            get => _fractalDepth;
            set
            {
                if (_fractalDepth == value)
                    return;
                _fractalDepth = value;

                OnPropertyChanged();
                RedrawCanvas();
            }
        }

        private string _fractalStartColor = "#FF003cb3";
        public string FractalStartColor
        {
            get => _fractalStartColor;
            set
            {
                if (_fractalStartColor == value)
                    return;
                _fractalStartColor = value;

                OnPropertyChanged();
                RedrawCanvas();
            }
        }

        private string _fractalEndColor = "#FF000000";
        public string FractalEndColor
        {
            get => _fractalEndColor;
            set
            {
                if (_fractalEndColor == value)
                    return;
                _fractalEndColor = value;

                OnPropertyChanged();
                RedrawCanvas();
            }
        }
        #endregion

        public FractalCreatorViewModel()
        {
            FractalTypes = new ObservableCollection<string>()
                { "T-square", "Cantor`s set", "Hilbert`s curve", "Sierpinski`s carpet" };
            CustomBox = new ObservableCollection<string>() { "1/3", "1/4", "1/5", "1/6", "1/7" };

            Canvas = new CanvasViewModel();
            Canvas.Size = CanvasSize;
            MessageQueue = new SnackbarMessageQueue(new TimeSpan(0, 0, 1));
            MessageQueue.IgnoreDuplicate = true;

            RedrawCanvas();
        }

        private void SaveToFile(object obj)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Png Image (.png)|*.png",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
            };

            if (saveFileDialog.ShowDialog() == false)
            {
                return;
            }

            var rtb = new RenderTargetBitmap(Canvas.Size, Canvas.Size,
                96d, 96d, PixelFormats.Default);
            rtb.Render(Canvas.Area);

            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

            using (var fs = File.OpenWrite(saveFileDialog.FileName))
            {
                pngEncoder.Save(fs);
            }
        }
        private static void SwitchToStart(object obj)
        {
            ViewSwitcher.Switch("Start");
        }

        private CancellationTokenSource _canvasCancelToken = new CancellationTokenSource();
        private void RedrawCanvas()
        {
            RedrawStarted();
            Fractal fractal = null;
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            switch (FractalTypes[FractalTypeIndex])
            {
                case "T-square":
                    fractal = new TSquare();
                    parameters.Add("startPosition", new PointF(CanvasSize / 4, CanvasSize / 4));
                    parameters.Add("sideLength", CanvasSize / 2);
                    break;
                case "Cantor`s set":
                    fractal = new CantorSet();
                    CustomBoxEnabled = true;
                    parameters.Add("rectangleStartWidth", CanvasSize);
                    parameters.Add("rectangleStartHeight", CanvasSize / 50);
                    parameters.Add("coefficient", CustomBoxIndex + 3);
                    break;
                case "Hilbert`s curve":
                    fractal = new HilbertCurve();
                    parameters.Add("size", CanvasSize);
                    break;
                case "Sierpinski`s carpet":
                    fractal = new SierpinskiCarpet();
                    parameters.Add("startPosition", new PointF(0, 0));
                    parameters.Add("sideLength", CanvasSize);
                    break;
            }

            Task.Run(() => ProgressChecker(fractal, _canvasCancelToken.Token), _canvasCancelToken.Token);
            Task.Run(() => RedrawImplementation(fractal, parameters, _canvasCancelToken.Token), _canvasCancelToken.Token).ContinueWith((task) => RedrawCompleted(), _canvasCancelToken.Token);
        }

        private void ProgressChecker(Fractal fractal, CancellationToken ct)
        {
            try
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                        ct.ThrowIfCancellationRequested();

                    Console.WriteLine(fractal.Progress);
                    ProgressValue = (int)(fractal.Progress * 100);
                    Thread.Sleep(10);
                }
            }
            catch (OperationCanceledException)
            {

            }
        }

        private void RedrawStarted()
        {
            _canvasCancelToken.Cancel();
            _canvasCancelToken = new CancellationTokenSource();
            CustomBoxEnabled = false;

            Canvas.Hide();
            ProgressOpacity = 1;
        }

        private void RedrawCompleted()
        {
            Canvas.Show();
            ProgressOpacity = 0;
        }

        private void RedrawImplementation(Fractal fractal, Dictionary<string, object> parameters, CancellationToken ct)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Canvas.Clear();
            });

            var startColor = (Color)ColorConverter.ConvertFromString(FractalStartColor);
            var endColor = (Color)ColorConverter.ConvertFromString(FractalEndColor);
            try
            {
                fractal.Draw(Canvas, FractalDepth, startColor, endColor, parameters, ct);
            }
            catch (OutOfMemoryException)
            {
                MessageQueue.Enqueue("Aborted by low memory");
            }
            catch (Exception)
            {

            }
        }

        #region INotify.

        /// <summary>
        /// INotify implementation.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        internal void OnPropertyChanged([CallerMemberName]string prop = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        #endregion
    }
}